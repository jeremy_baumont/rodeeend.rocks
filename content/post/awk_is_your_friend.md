+++
date = "2016-04-15T16:41:39+01:00"
draft = false 
title = "Awk is your friend"
description = "Awk is an amazing operation tool."
tags = [ "awk", "ops", "tool", "unix", "log" ]
topics = [ "awk", "ops" ]
slug = "ops"

+++

## 9-iron

At 14 years old, on Spring holidays, I was sent by my parents to a tennis boot camp, I was scheduled to do 2 weeks intensive training. I started at this time to be sick of playing tennis. So I successfully convinced them to let me do a golf initiation course the second week. It was fun to try a new sport. I remember the first lesson where the teacher asked us to take one and only one club to execute a 9 holes course. I choose obviously one of those wood club probably biased by [Goofy's image playing golf](http://a.dilcdn.com/bl/wp-content/uploads/sites/2/2014/08/golf.jpg). The teacher took a 9-iron. It was the best trade off from the teeing ground to the putting green even if you had to pass by a bunker. The lesson was to appreciate the values of your tools, implicitly meaning the [pareto principle](http://en.wikipedia.org/wiki/Pareto_principle). If you need to invest 20% of your time to make 80% of results: choose wisely your tool.

I like to think that awk is my 9-iron. It is a great tool I use daily for many different kind of troubleshooting. I am learning every day new trick and it saves me a lot of hassle. 

Awk is made to search files for lines (awk gives you the liberty to define the granularity that best suits your search so not only lines) that include specific patterns. When a line matches one of those patterns, awk executes associated actions on that line then it continues to process the rest of the file the same way. I won't go further explaining how patterns and actions work because there are plenty of excellent [Awk blog posts](http://http://awk.info/?Learn) out there that does the job perfectly, personnaly I would recommand the [awk in 20 minutes](htp://ferd.ca/awk-in-20-minutes.html) to get started. Just basic principles should be enough to follow the rest of this article.

## If only

I recently moved down under from Europa and the lack of standard in different
industries is always a burden: voltage adaptor, cup to gram conversion table, 
specific cable to load its battery... The
list is long, I know it is about history and heritage and you cannot ask to
change all electrical cables neither to change all weight and distance
metrics boards, apparels and devices. Date format is also a
good source of headache, notably the conversion epoch time to usual date format.

```
> tail -5 nagios.log

[1424893454] SERVICE NOTIFICATION: PROC CRITICAL Expected at least 1 chef-client process, but found 0
[1424893455] SERVICE NOTIFICATION: CRITICAL: DOM_Temperature_Sensor_for_Ethernet19 = 0 Celsius
[1424893456] Warning: Return code of 255 for check of service 'cpu' on host 'DB771' was out of bounds.
[1424893482] Warning: Return code of 255 for check of service 'cpu' on host 'DB171' was out of bounds.
[1424893484] Warning: Return code of 255 for check of service 'eventlog' on host 'DB771' was out of bounds.
```

```
> tail -5 nagios.log | awk -F'[\\[\\]]' \
> '{  cmd = "/bin/date -d @"$2" +%Y%m%d_%H:%M" ; \
> $1=$2=""; \
> if (cmd | getline var ) print var, $0; \
>  close(cmd) }'

20150225_20:44    SERVICE NOTIFICATION: PROC CRITICAL Expected at least 1 chef-client process, but found 0
20150225_20:44    SERVICE NOTIFICATION: CRITICAL: DOM_Temperature_Sensor_for_Ethernet19 = 0 Celsius
20150225_20:44    Warning: Return code of 255 for check of service 'cpu' on host 'DB771' was out of bounds.
20150225_20:44    Warning: Return code of 255 for check of service 'cpu' on host 'DB171' was out of bounds.
20150225_20:44    Warning: Return code of 255 for check of service 'eventlog' on host 'DB771' was out of bounds.
```

Line 9 define actions that will be executed before reading any input, it
specifies here the ```FS``` field separator with a regular expression that
means either the opening or closing brackets character: *'['* or *']'*. So the
lines will be cut in fields every time awk meets those 2 characters and
that is because the epoch time in nagios log format is between those characters,
we will always find it in the second field defined by built-in variable ```$2```. 

Line 10 to 13 define a rule, where the pattern is absent so this means it will
match every line and associated actions will be applied to every lines. Line 12 
is a [well-known tip](http://awk.info/?tip/getline) in order to execute a
command on a specific field, the common canvat for getline is:
```
cmd="some command" 
do something with cmd 
close(cmd)
```

So we extract the epoch time in ```$2```, compute a human readable date with ```cmd```, print
it with ```var``` and the rest of the line with ```$0``` where we took care to
empty ```$1``` and ```$2```.


## Links:
* [Awk blog post](http://ferd.ca/awk-in-20-minutes.html) that gives you the essentials to get you started with awk. 
* [Gawk FSF manual](https://www.gnu.org/software/gawk/manual/gawk.html) is a must read is you are enthusiast and serious with awk.
* [Awk related website](http://awk.info/) with plenty of resources.
