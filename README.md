# blog

Rodeend.rock's blog built with [hugo](http://gohugo.io/) static site generator.

# Usage

* Create the Markdown source for the new post within the content/posts directory
```vi content/posts/awk_is_your_friend.md```

* Preview your work by running Hugo in server mode with hugo server --watch
```hugo server --theme=ghostwriter --buildDrafts --watch=true```

* Run **deploy.sh** :
```./deploy.sh Add a new post about how I use awk daily```
    * Run Hugo not in server mode so that the generated urls will be correct for the website
    * Add and commit the new post in master branch
    * Push the master branch
