#!/usr/bin/env bash

GIT_OWNER="jeremy_baumont"
GIT_REPO="rodeeend.rocks"

echo -e "\033[0;32mDeploying updates to bitbucket https://bitbucket.org/${GIT_OWNER}/${GIT_REPO}...\033[0m"
 
# Build the project.
hugo --theme=ghostwriter --buildDrafts
 
# Add changes to git.
git add -A
 
# Commit changes.
msg="Rebuilding blog site `date`"
if [ $# -gt 0 ]
  then msg="$*"
fi
git commit -m "$msg"
 
# Push source and build repos.
git push origin-bitbucket master
#git subtree push --prefix=public git@github.com:${GIT_OWNER}/${GIT_REPO}.git gh-pages
